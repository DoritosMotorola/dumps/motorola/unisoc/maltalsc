#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/soc/soc.ap-ahb/20600000.sdio/by-name/recovery:41943040:4b74446d7e9d484073ddea41b66c5a8b98fef939; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/soc/soc.ap-ahb/20600000.sdio/by-name/boot:36700160:97ca61ddbd612306444e8aa496366207d02a2d31 \
          --target EMMC:/dev/block/platform/soc/soc.ap-ahb/20600000.sdio/by-name/recovery:41943040:4b74446d7e9d484073ddea41b66c5a8b98fef939 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
